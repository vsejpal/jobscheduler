﻿using System;
using System.Collections.Generic;
using System.Linq;
using JobScheduler.Models;

namespace JobScheduler

{
    class JobScheduler {
        private readonly SchedulerContext _context;
        private readonly int maxProcessorCount = 9; // random number
        private readonly TimeSpan coolOffPeriod = new TimeSpan(4, 0, 0); // all dependencies should be run within last 4 hours
        Queue<Job> queue; 

        public JobScheduler(SchedulerContext context) {
            _context = context;
            queue = new Queue<Job>(); // in production this would be rabbitMQ, SQS or similar
        }
        public DateTime Schedule(JobType type, DateTime deadLine, List<Job> dependencies) {
            //validate dependencies
            var allJobs = from j in _context.Jobs
                          select j;  // get all jobs from 'Job' table
            bool isSubset = !dependencies.Except(allJobs).Any(); // is `dependencies` a `subset` of allJobs?

            if (!isSubset) {
                throw new ArgumentException("some dependencies are not in the master `Job` table");
            }


            // get the flatted, ordered list of dependencies to be run first
            List<Job> dependenciesToBeRun = GetAllDependenciesToRun(dependencies);

            TimeSpan dependencyEta = CalculateCumulativeEta(dependenciesToBeRun);

            // add the ETA for `this` task at hand to `dependencyEta`. 
            // if the queue was empty (because each consumer is consuming fast enough), then `jobEta` would be our result.
            TimeSpan jobEta = dependencyEta + GetEtaTable()[type];



        }

        private List<Job> GetAllDependenciesToRun(List<Job> dependencies) {
            /*
             * This method magically returns all dependent chilren, grand-children
             * that has not been run long enough since the cool-off period
             * and the return list is ordred in a way that the lowest level job
             * that needs to be run, appears first.
             */
            throw new NotImplementedException();
        }

        private TimeSpan CalculateCumulativeEta(List<Job> jobs) {
            TimeSpan result = TimeSpan.Zero;

            foreach (Job job in jobs) {
                result += GetEtaTable()[job.Type];
            }

            return result;
        }

        private Dictionary<JobType, TimeSpan> GetEtaTable() {
            Dictionary<JobType, TimeSpan> etaTable = new Dictionary<JobType, TimeSpan>() {
                {JobType.CPU, new TimeSpan(2,0,0)}, // CPU jobs take 2 hours
                {JobType.IO, new TimeSpan(1,0,0)}, // IO jobs take 1 hour
                {JobType.X, new TimeSpan(0,30,0)} // X type jobs take 30 mins
            };

            return etaTable;
        }


    }
}
