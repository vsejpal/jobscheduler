﻿using System;
using Microsoft.EntityFrameworkCore;
namespace JobScheduler.Models
{
    public class SchedulerContext : DbContext
    {
        public SchedulerContext(DbContextOptions<SchedulerContext> options)
            :base(options)
        {
        }

        public DbSet<Job> Jobs { get; set; }
    }

}
