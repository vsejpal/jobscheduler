﻿using System;
using System.Collections.Generic;

namespace JobScheduler.Models
{
    public enum JobType { CPU, IO, X, Y, Z }
    public enum JobRunStatus { Running, Queued, Completed, Cancelled }

    public class Job
    {
        public Guid Id { get; set; } // auto-assigned by DB on persistance
        public JobType Type { get; set; }
        public List<Job> DependsOn { get; set; } // there will be a one-many table in DB to store this one-many relationship

        public Job(JobType type, List<Job> dependencies)
        {
            Type = type;
            DependsOn = dependencies;
        }
    }
}
